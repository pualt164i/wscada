import os
from channels.asgi import get_channel_layer

from .settings.get_env import get_env_variable

STATUS=get_env_variable('STATUS')


os.environ.setdefault("DJANGO_SETTINGS_MODULE", STATUS)

channel_layer = get_channel_layer()


#daphne my_project.asgi:channel_layer
