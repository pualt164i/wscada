"""
WSGI config for rtviz project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
from .settings.get_env import get_env_variable

from django.core.wsgi import get_wsgi_application


STATUS=get_env_variable('STATUS')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", STATUS)

application = get_wsgi_application()
