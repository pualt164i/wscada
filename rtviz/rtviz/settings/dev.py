# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

# -*- coding: utf-8 -*-
from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*','dpineda.csn.uchile.cl']

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': get_env_variable('DBNAME'),
        'USER': get_env_variable('DBUSER'),
        'PASSWORD': get_env_variable('DBPASS'),
        'HOST': get_env_variable('DBHOST'),
        'PORT': get_env_variable('DBPORT'),
    }
}
