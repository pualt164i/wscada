# In routing.py
from channels.routing import route
from channels.routing import include

# from apps:
from apps.sci_input.routing import sci_routing
from apps.charts.routing import charts_routing
from apps.testws.routing import testws_routing
from apps.consulta_station.routing import consulta_routing

channel_routing = [
    include(sci_routing, path=r"^/sci_input"),
    include(sci_routing, path=r"^/charts"),
    include(testws_routing, path=r"^/test"),
    include(consulta_routing, path=r"^/consulta")
]
