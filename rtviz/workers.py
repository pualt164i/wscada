import asyncio
import functools
import sys
import subprocess
from networktools.colorprint import gprint, bprint, rprint
from networktools.environment import get_env_variable
from multiprocessing.managers import BaseManager
from multiprocessing import Manager, Queue
import concurrent.futures


#import matplotlib.animation as animation
import numpy as np

#from charts.seno_animado import Scope

def load_worker():
    command=["./manage.py" , "runworker", "-v 2"]
    sprocess=subprocess.run(command)
    return sprocess


if __name__ == "__main__":
    loop=asyncio.get_event_loop()
    workers=int(get_env_variable("RTVIZ_WORKERS"))
    executor=concurrent.futures.ProcessPoolExecutor(workers)
    tasks_list=[]
    for i in range(workers):
        task_process=loop.run_in_executor(
            executor,
            load_worker)
        tasks_list.append(task_process)
    loop.run_until_complete(asyncio.gather(*tasks_list))
