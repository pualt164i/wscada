from django.apps import AppConfig


class SetChartsConfig(AppConfig):
    name = 'set_charts'
