# In routing.py
from channels.routing import route, route_class
from apps.datastorage.consumers import StationDataConsumer, DataDemultiplexer

data_routing = [
    StationDataConsumer.as_route(path=r"^/data/", attrs={'group':'data-consumer'}),
    route_class(DataDemultiplexer, path="^/data-stream/")
]
