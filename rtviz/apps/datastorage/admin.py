from django.contrib import admin

# Register your models here.
from apps.datastorage.models import StationData

class StationDataAdmin(admin.ModelAdmin):
    fields=('station_name','timestamp','source','data')

admin.site.register(StationData, StationDataAdmin)
