from django.db import models
from django.contrib.postgres.fields import JSONField
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save

import datetime
# Create your models here.

class StationData(models.Model):
    source=models.CharField(max_length=60)
    station_name=models.CharField(max_length=10)
    timestamp=models.DateTimeField(default=datetime.datetime.utcnow)
    data=JSONField(default={})
    #last_update=models.DateTimeField(default=datetime.datetime.utcnow)

    def __str__(self):
        return "%s : %s" %(self.station_name, self.timestamp)

    class Meta:
        verbose_name="Station Data"
        verbose_name_plural="Stations Data"

