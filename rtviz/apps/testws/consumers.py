from channels.handler import AsgiHandler

from channels import Channel, Group
from channels.sessions import channel_session
from channels.auth import channel_session_user, channel_session_user_from_http

from channels.generic.websockets import JsonWebsocketConsumer

from networktools.colorprint import bprint, gprint, rprint
#from networktools.geo import ecef2llh , llh2ecef

from Ejecuta.obtiene_extraccion import Ejecuta
from Extraer.extraer import Extract, main

import asyncio

import pyproj

from django.core import serializers

import simplejson as json

from .models import TestWS

from .bindings import TestWSBinding

from collector.orm.manager import SessionCollector

import re

class TestWSConsumer(JsonWebsocketConsumer):
    #channel_session_user = True
    strict_ordering=False
    channel_session = True


    def setCollector(self):
        self.collector = SessionCollector()
    

    def connect(self, message, **kwargs):
	
        self.setCollector()

        multiplexer=kwargs['multiplexer']
        rprint("Enviando aprobacion")
        

        #multiplexer.send({"accept": True,'kk':estaciones})
        #multiplexer.send({'accept':True})
        #msg = {"stream":'testws','payload': 'xxx'}
        #multiplexer.send(msg)
        #super(CSKConsumer,self).connect(message,*args,**kwargs)
        #super(CSKConsumer,self).connect(message, multiplexer,**kwargs)
        ##self.raw_receive(message, multiplexer,**kwargs)

    def connection_groups(self, **kwargs):
        """
        Return the list of allowed groups
        """
        return ["testws","testws_stream-updates"]

    def disconnect(self, message, **kwargs):
        
        multiplexer=kwargs['multiplexer']
        bprint("Stream %s is closed" % multiplexer.stream)
    
    def raw_receive(self, message,  **kwargs):
        # bprint(message)
        if "text" in message:
            self.receive(self.decode_json(message['text']), **kwargs)
        else:
            raise ValueError("No text section for incoming WebSocket frame!")
    
    def receive(self, content, **kwargs):
        """
        Called when a message is received with decoded JSON content
        """

        #bprint(content)
        multiplexer=kwargs['multiplexer']
        s=SessionCollector()

        ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
        lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')

        bprint(content)

        if content['command']=='enviar':
               
            if (content['message'])['code']=='':    # Codigo de busqueda
 
                station = s.get_stations() 

                for s in station:
                
                    lon, lat, alt = pyproj.transform(ecef, lla, float(s.position_x),float(s.position_y),float(s.position_z), radians=False)
                
                    stat = {"command":"enviar","code":s.code,"name":s.name, "x":lon, "y":lat, "z":alt, "check": False}
                    msg = {"stream":'testws','payload':stat}
                    # bprint(msg)
                    multiplexer.send(msg)
            else:

                pk = s.get_station_id((content['message'])['code'])
                station = s.get_station_by_id(pk)
                if station is None:
                    pass
                else:   
                    lon, lat, alt = pyproj.transform(ecef, lla, float(station.position_x),float(station.position_y),float(station.position_z), radians=False)
                    stat = {"command":"enviar","code":station.code,"name":station.name, "x":lon, "y":lat, "z":alt, "check": False}
                    msg = {"stream":'testws','payload':stat}
                    # bprint(msg)
                    multiplexer.send(msg)
             

        elif content['command']=='pull':
                # '2017/11/17 06:32 +0000'
                fecha_ini = (content['message'])['fecha_ini']
                #fecha_ini = re.sub(r'-', '/', fecha_ini)
                tkey = "%sT%s:00" % (fecha_ini, (content['message'])['hora_ini']) 
                #re.sub(r'-', '\/', tkey)
                              
                stations = (content['message'])['stations']
                
                for e in stations:
                    bprint(e)

                    loop = asyncio.new_event_loop()
                    asyncio.set_event_loop(loop)

                    eje=Ejecuta()
                    
                    #eje.ejecuta('VALN','2018-01-04T00:01:12')
                    csvdir = eje.ejecuta(e,tkey)
                    del eje

                    msg = {"stream":'testws','payload':{"command":"pull","data":csvdir}}
                    multiplexer.send(msg)
                    bprint(msg)
                    
                    #del ext

        #msg = {"stream":'testws','payload':'Super Uli'}
        #bprint(msg)
        #multiplexer.send(msg)
        
        



from channels.generic.websockets import  WebsocketDemultiplexer, JsonWebsocketConsumer


class Demultiplexer(WebsocketDemultiplexer):

    # Wire your JSON consumers here: {stream_name : consumer}
    consumers = {
        'testws':TestWSConsumer,
    }

    # Optionally provide a custom multiplexer class
    # multiplexer_class = MyCustomJsonEncodingMultiplexer

    def connection_groups(self):
        return ['testws_stream-updates']
