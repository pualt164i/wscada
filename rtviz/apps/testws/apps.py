from django.apps import AppConfig


class TestwsConfig(AppConfig):
    name = 'testws'
