from django.apps import AppConfig


class SciInputConfig(AppConfig):
    name = 'sci_input'
    verbose_name="Scientific Data Inputs"
