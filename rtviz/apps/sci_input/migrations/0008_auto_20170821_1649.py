# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-21 16:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sci_input', '0007_auto_20170707_1835'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datatype',
            name='description',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='datatype',
            name='graph_d3',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='datatype',
            name='graph_dot',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='datatype',
            name='graph_json',
            field=models.TextField(default=''),
        ),
    ]
