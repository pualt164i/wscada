from ajax_select import register, LookupChannel
from apps.sci_input.models import DataType

from scigrammar.walnut import Walnut, genpaths
from scigrammar.brackets import parse2wnbank, WnAssemble, create_wn_all


@register("check_csk")
class CSK(LookupChannel):
    model= DataType

    def check_csk(self, csk):
        wn=create_wn_all(csk)
        return wn.dot.source
