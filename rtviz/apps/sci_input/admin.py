from django.contrib import admin
from apps.sci_input.models import DataType
from ajax_select import make_ajax_form
# Register your models here.


@admin.register(DataType)
class DataTypeAdmin(admin.ModelAdmin):
    fields=('name','csk','description')

    def check_csk(self, request):
        response_dict={
            'success':True,
            'graph':''
        }

        if request.is_ajax() and request.method=='POST':
            csk=request.POST.get('csk','')
            wn=create_wn_all(csk)
            response_dict['graph']=wn.getd3graph()
            response_dict['size']=wn.size

        return JsonResponse(response_dict,safe=False)
