from django.db import models
from apps.sci_input.fields import WalnutField
# Create your models here.
from django.contrib.postgres.fields import ArrayField
from scigrammar.brackets import create_wn_all


class DataType(models.Model):
    """
    :model:`sci_input.DataType` 
    This model can manage different data inputs structures
    defined by a Compact String Keys (CSK).
    With that CSK, you can define the input dictionary with some
    defined keys.
    This can give you a Walnut Tree, and with that you can obtain a graph and
    the list of different paths to the data.
    """
    TYPE_CHOICES = (
        ('std','Standar'),
        ('ptr','Particular')
    )
    name= models.CharField(max_length=20)
    csk= WalnutField(max_length=300)
    graph_dot=models.TextField(default="")
    graph_json=models.TextField(default="")
    graph_d3=models.TextField(default="")
    description=models.TextField(default="")
    type_data_input=models.CharField(
        max_length=3,
        choices=TYPE_CHOICES,
        default='ptr'
    )

    #Validate CSK

    def save(self, *args, **kwargs):
        wn=create_wn_all(self.csk)
        d3_graph=wn.getd3graph()
        dot=wn.createdot()
        code=wn.codeid()
        wn.getgraphviz(code,dot)
        dot_graph=wn.dot.source
        json_graph=wn.getjsongraph()
        self.graph_dot=dot_graph
        self.graph_d3=d3_graph
        self.grap_json=json_graph
        super(DataType,self).save(*args,**kwargs)

    @property
    def type_name(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        app_label="sci_input"
        db_table="SCI_INPUT_datatype"
        verbose_name='Data Type'
        verbose_name_plural = 'Data Types'
        ordering = ("name",)

class DataTypePath(models.Model):
    """
    :model: 'sci_input.DataTypePath'
    That model extract the paths from the walnut generated by the csk and save
    every path on a diferent instance related to the DataType
    When a new DataType is created, the DataTypePath is filled, when is updated,
    the old DataTypePaths are erased and renewed,
    """
    datatype=models.ForeignKey(
        DataType,
        blank=True,
        null=True,
        on_delete=models.CASCADE
    )
    path=ArrayField(models.CharField(max_length=30, blank=True))

    @property
    def type_path(self):
        return "%s:%s" % (self.datatype.name, self.pk)

    def __str__(self):
        return self.type_path()

    class Meta:
        app_label="sci_input"
        db_table="SCI_INPUT_datatype_path"
        verbose_name='Data Type Path'
        verbose_name_plural = 'Data Type Paths'
        ordering = ("datatype",)


class DataTypeSocket(models.Model):
    """
    :model: 'sci_input.DataTypeSocket'
    This model allow you to define connections between a Standar Datatype
    that can be used to define generic charts and a Particular Datatype that
    can be used to define different kinds of dynamic data
    :particular: output socket
    :standar: input socket

    """
    particular=models.ForeignKey(
        DataType,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="particular_path",
        limit_choices_to={'datatype.type_data_input':'ptr'})
    standar=models.ForeignKey(
        DataType,
        blank=True,
        null=True,
        related_name='standar_path',
        on_delete=models.CASCADE,
        limit_choices_to={'datatype.type_data_input':'std'})


    @property
    def socket_name(self):
        return "%s with %s" % (self.particular.type_path(), self.standar.type_path())

    def __str__(self):
        return self.socket_name()

    class Meta:
        app_label="sci_input"
        db_table="SCI_INPUT_datatype_socket"
        verbose_name='Data Type Socket'
        verbose_name_plural = 'Data Type Sockets'


