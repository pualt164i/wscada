//SOURCE:

//https://bl.ocks.org/d3noob/43a860bc0024792f8803bba8ca0d5ecd

//DEFINE CANVAS
console.log("D3 TREE LOADED")
console.log(d3)


treeData={
  "name": "SIN",
  "children": [
    {
      "name": "CSK"
	  }
  ]
}

console.log(treeData)

var margin = {top: 20, right: 120, bottom: 20, left: 120},
    width = 600 - margin.right - margin.left,
    height = 300 - margin.top - margin.bottom;

var i = 0,
    duration = 750,
    root;

//Ok on version v3 but...
/*var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });
*/

// Better for v4_

var svg = d3.select("#walnut_graph").append("svg")
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


var treemap = d3.tree();

// Assigns parent, children, height, depth
// hierarchy(data, children)
root = d3.hierarchy(treeData, function(d) { return d.children; });
root.x0 = height / 2;
root.y0 = 0;

// Collapse after the second level
root.children.forEach(collapse);


var d3size={'height':300,'width':300}

update(root);

// Collapse the node and all it's children
function collapse(d) {
  if(d.children) {
    d._children = d.children
    d._children.forEach(collapse)
    d.children = null
  }
}

d3.select(self.frameElement).style("height", "200px");

function resize(new_size){

  // RESIZE
  d3size=new_size
  console.log("Redefine size:")
  console.log(d3size)
  console.log(new_size)
  treemap.size([new_size['height'],new_size['width']])

  var container=d3.select("#walnut_graph svg")

  container
    .attr('width',new_size['width']+margin.left+margin.right)
    .attr('height',new_size['height']+margin.bottom+margin.top)
}

resize(d3size)

function update(source) {
  //
  var treeData=treemap(root)

  // Compute the new tree layout.
  var nodes = treeData.descendants(),
      links =  treeData.descendants().slice(1)


  console.log("Nodes array")

  console.log(nodes)

  console.log("Slice(1)")
  console.log(links)

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 180; });

  // Update the nodes…
  var node = svg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
      .on("click", click);

  // Add Circle for the nodes
  nodeEnter.append('circle')
      .attr('class', 'node')
      .attr('r', 10)
      .style("fill", function(d) {
          return d._children ? "lightsteelblue" : "#fff";
      });

  
  // Add labels for the nodes
  nodeEnter.append('text')
      .attr("dy", ".35em")
      .attr("x", function(d) {
          return d.children || d._children ? -13 : 13;
      })
      .attr("text-anchor", function(d) {
          return d.children || d._children ? "end" : "start";
      })
    .text(function(d) { return d.data.name; });

  // Transition nodes to their new position.
  var nodeUpdate = nodeEnter.merge(node)


  // Transition to the proper position for the node
  nodeUpdate.transition()
    .duration(duration)
    .attr("transform", function(d) { 
        return "translate(" + d.y + "," + d.x + ")";
     });

    // Update the node attributes and style
  nodeUpdate.select('circle.node')
    .attr('r', 10)
    .style("fill", function(d) {
        return d._children ? "lightsteelblue" : "#fff";
    })
    .attr('cursor', 'pointer');

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .remove();

  nodeExit.select("circle")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

  // Update the links...
  var link = svg.selectAll('path.link')
        .data(links, function(d) { return d.id; });


  // Enter any new links at the parent's previous position.
    var linkEnter = link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal(o,o);
      });


  // UPDATE
  var linkUpdate = linkEnter.merge(link);


  // Transition back to the parent element position
  linkUpdate.transition()
      .duration(duration)
      .attr('d', function(d){ return diagonal(d, d.parent) });


  // Remove any exiting links
  var linkExit = link.exit().transition()
      .duration(duration)
      .attr('d', function(d) {
        var o = {x: source.x, y: source.y}
        return diagonal(o, o)
      })
      .remove();
  
  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });


  // Creates a curved (diagonal) path from parent to the child nodes
  function diagonal(s, d) {
    path=null

    if(d){

    path = `M ${s.y} ${s.x}
            C ${(s.y + d.y) / 2} ${s.x},
              ${(s.y + d.y) / 2} ${d.x},
              ${d.y} ${d.x}`
    }

    return path
  }

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}

}
