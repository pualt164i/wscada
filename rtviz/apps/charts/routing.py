from channels.routing import route, route_class
from apps.charts.consumers import ChartConsumer, ChartCreatorConsumer, ChartsDemultiplexer

charts_routing = [
    ChartConsumer.as_route(path=r"^/chart/", attrs={'group':'chart-updates'}),
    ChartCreatorConsumer.as_route(path=r"^/chart_creator/", attrs={'group':'chart_creator-updates'}),
    route_class(ChartsDemultiplexer, path='^/binding/')
]
