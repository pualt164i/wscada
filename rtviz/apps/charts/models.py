from django.db import models
from django_file_validator.validators import MaxSizeValidator
import xml.etree.cElementTree as et
from phonenumber_field.modelfields import PhoneNumberField
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from apps.sci_input.models import DataType
#arrayfields
from django.contrib.postgres.fields import ArrayField

import datetime

# Create your models here.
def validate_svg(filename):
    tag = None
    with open(filename, "r") as f:
        try:
            for event, el in et.iterparse(f, ('start',)):
                tag = el.tag
                break
        except et.ParseError:
            raise et.ParseError
        except Exception:
            raise Exception
    return tag == '{http://www.w3.org/2000/svg}svg'

class ChartCreator(models.Model):
    name=models.CharField(max_length=50)
    email=models.EmailField()
    phone=PhoneNumberField()
    url=models.URLField(max_length=300)

    @property
    def creator(self):
        return self.name

    @property
    def email(self):
        return self.email

    @property
    def url(self):
        return self.url

    @property
    def phone(self):
        return self.phone

    def __str__(self):
        return self.creator()

    class Meta:
        verbose_name='Creator\'s Chart'
        verbose_name_plural='Creators\' Charts'


class Chart(models.Model):
    name=models.CharField(max_length=20)
    description=models.TextField()
    giturl=models.URLField(max_length=300)
    creator=models.ForeignKey(
        ChartCreator,
        models.SET_NULL,
        blank=True,
        null=True,
        limit_choices_to={'type_data_input':'std'})
    inputs=ArrayField(models.CharField(max_length=20, blank=True))
    properties=ArrayField(models.CharField(max_length=30, blank=True))
    #only admit svg icons
    icon=models.FileField(
        upload_to='icons/',
        validators=[
            validate_svg,
            MaxSizeValidator(1024)
        ])
    data_input_standar=models.ForeignKey(
        DataType,
        models.SET_NULL,
        blank=True,
        null=True)
    last_update=models.DateTimeField(blank=True)
    js_function=models.CharField(max_length=30, blank=True)
    js_library=models.CharField(max_length=30, blank=True)


    @property
    def chart(self):
        return self.name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name="Chart"
        verbose_name_plural="Charts"



# Set active_from if active is set in an update
@receiver(pre_save, sender=Chart)
def set_active_from_on_update(sender, instance, update_fields, **kwargs):
    if 'is_active' in update_fields and instance.is_active is True:
            instance.last_update = datetime.now()

# Set active_from if active is set on create
@receiver(post_save, sender=Chart)
def set_active_from_on_create(sender, instance, created, **kwargs):
    if created and instance.is_active is True:
        instance.last_update = datetime.now()
