from django.http import HttpResponse
from channels.handler import AsgiHandler

from channels import Channel, Group
from channels.sessions import channel_session
from channels.auth import channel_session_user, channel_session_user_from_http

from channels.generic.websockets import JsonWebsocketConsumer

from channels.generic.websockets import WebsocketDemultiplexer, \
    JsonWebsocketConsumer

# import binding
from apps.charts.bindings import ChartBinding, ChartCreatorBinding

# import models
from apps.charts.models import Chart, ChartCreator

from django.core import serializers

import simplejson as json



# Connected to websocket.connect
class ChartConsumer(JsonWebsocketConsumer):
    #channel_session_user = True
    strict_ordering=False
    channel_session = True

    def raw_receive(self, message, **kwargs):
        bprint(message)
        rprint(message['text'])
        super(CSKConsumer, self).raw_receive(message, **kwargs)

    def receive(self, content, **kwargs):
        """
        Called when a message is received with decoded JSON content
        """
        bprint(content)
        response=None
        self.send(response)

# Connected to websocket.connect
class ChartCreatorConsumer(JsonWebsocketConsumer):
    #channel_session_user = True
    strict_ordering=False
    channel_session = True

    def raw_receive(self, message, **kwargs):
       bprint(message)
       rprint(message['text'])
       super(CSKConsumer, self).raw_receive(message, **kwargs)

    def receive(self, content, **kwargs):
        """
        Called when a message is received with decoded JSON content
        """
        bprint(content)
        response=None
        self.send(response)


class ChartsDemultiplexer(WebsocketDemultiplexer):

    # Wire your JSON consumers here: {stream_name : consumer}
    consumers = {
        #"echo": ConsumerCSK_INPUT,
        #"other": ConsumerCSK_INPUT_2,
        'charts':ChartBinding.consumer,
        'creators':ChartCreatorBinding.consumer,
    }

    # Optionally provide a custom multiplexer class
    # multiplexer_class = MyCustomJsonEncodingMultiplexer

    def connection_groups(self):
        return ['chart_creator-updates','chart-updates']
