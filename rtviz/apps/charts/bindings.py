from apps.charts.models import Chart, ChartCreator
from channels.binding.websockets import WebsocketBinding


class ChartBinding(WebsocketBinding):
    model=Chart
    stream='chart'
    fields=[
        'name',
        'giturl',
        'description',
        'inputs',
        'properties',
        'icon',
        'data_input_standar'
    ]

    @classmethod
    def group_names(cls, instance):
        return ['chart-updates']

    def has_permission(self, user, action, pk):
        return True


class ChartCreatorBinding(WebsocketBinding):
    model=ChartCreator
    stream='chart_creator'
    fields=[
        'name',
        'email',
        'phone',
        'url'
    ]

    @classmethod
    def group_names(cls, instance):
        return ['chart_creator-updates']

    def has_permission(self, user, action, pk):
        return True
